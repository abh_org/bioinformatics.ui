/**
 * Copyright (C) 2016, Antony Holmes
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *  3. Neither the name of copyright holder nor the names of its contributors 
 *     may be used to endorse or promote products derived from this software 
 *     without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.abh.common.bioinformatics.ui.groups;

import java.awt.Color;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Box;

import org.abh.common.bioinformatics.ext.ucsc.Bed;
import org.abh.common.bioinformatics.ext.ucsc.BedGraph;
import org.abh.common.bioinformatics.ext.ucsc.UCSCTrack;
import org.abh.common.bioinformatics.ext.ucsc.UCSCTrackRegion;
import org.abh.common.bioinformatics.file.BioPathUtils;
import org.abh.common.bioinformatics.genomic.GenomicRegion;
import org.abh.common.bioinformatics.ui.external.ucsc.BedGraphGuiFileFilter;
import org.abh.common.bioinformatics.ui.external.ucsc.BedGuiFileFilter;
import org.abh.common.bioinformatics.ui.genome.RegionsTextArea;
import org.abh.common.math.external.microsoft.Excel;
import org.abh.common.math.ui.external.microsoft.AllXlsxGuiFileFilter;
import org.abh.common.math.ui.external.microsoft.XlsxGuiFileFilter;
import org.abh.common.ui.BorderService;
import org.abh.common.ui.ModernComponent;
import org.abh.common.ui.UIService;
import org.abh.common.ui.dialog.ModernDialogFlatButton;
import org.abh.common.ui.event.ModernClickEvent;
import org.abh.common.ui.event.ModernClickListener;
import org.abh.common.ui.graphics.color.ColorSwatchButton;
import org.abh.common.ui.graphics.icons.OpenFolderVectorIcon;
import org.abh.common.ui.io.FileDialog;
import org.abh.common.ui.io.RecentFilesService;
import org.abh.common.ui.io.TxtGuiFileFilter;
import org.abh.common.ui.panel.HBox;
import org.abh.common.ui.panel.MatrixPanel;
import org.abh.common.ui.scrollpane.ModernScrollPane;
import org.abh.common.ui.scrollpane.ScrollBarPolicy;
import org.abh.common.ui.text.ModernAutoSizeLabel;
import org.abh.common.ui.text.ModernClipboardTextField;
import org.abh.common.ui.text.ModernTextBorderPanel;
import org.abh.common.ui.text.ModernTextField;
import org.abh.common.ui.widget.ModernWidget;
import org.abh.common.ui.window.ModernWindow;

// TODO: Auto-generated Javadoc
/**
 * Allows a matrix group to be edited.
 *
 * @author Antony Holmes Holmes
 */
public class GroupEditPanel extends ModernComponent implements ModernClickListener {
	
	/**
	 * The constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * The member color button.
	 */
	private ColorSwatchButton mColorButton;

	/**
	 * The member name field.
	 */
	private ModernTextField mNameField = 
			new ModernClipboardTextField("Name");

	/**
	 * The member group.
	 */
	private Group mGroup;
	
	/**
	 * The member text area.
	 */
	private RegionsTextArea mTextArea = new RegionsTextArea();

	/**
	 * The member parent.
	 */
	private ModernWindow mParent;
	
	/**
	 * The member load button.
	 */
	private ModernDialogFlatButton mLoadButton =
			new ModernDialogFlatButton("Load...", UIService.getInstance().loadIcon(OpenFolderVectorIcon.class, 16));
	
	/**
	 * Instantiates a new group edit panel.
	 *
	 * @param parent the parent
	 * @param group the group
	 */
	public GroupEditPanel(ModernWindow parent, Group group) {
		mParent = parent;
		mGroup = group;
		
		mNameField.setText(group.getName());
		//this.getContentPane().add(new JLabel("Change " + getProductDetails().getProductName() + " settings", JLabel.LEFT), BorderLayout.PAGE_START);

		int[] rows = {ModernWidget.WIDGET_HEIGHT};
		int[] cols = {80, 300};
		
		MatrixPanel matrixPanel = new MatrixPanel(rows, 
				cols, 
				ModernWidget.PADDING, 
				ModernWidget.PADDING);
		
		mColorButton = new ColorSwatchButton(parent, 
				mGroup.getColor());

		matrixPanel.add(new ModernAutoSizeLabel("Name"));
		matrixPanel.add(new ModernTextBorderPanel(mNameField));
		matrixPanel.add(new ModernAutoSizeLabel("Color"));
		
		Box box = HBox.create();
		box.add(mColorButton);
		
		matrixPanel.add(box);
		
		matrixPanel.setBorder(BorderService.getInstance().createBottomBorder(10));

		setHeader(matrixPanel);
		
		ModernScrollPane scrollPane = new ModernScrollPane(mTextArea)
				.setVerticalScrollBarPolicy(ScrollBarPolicy.ALWAYS);
		
		setBody(scrollPane);
		
		box = HBox.create();
		
		box.setBorder(TOP_BORDER);
		
		box.add(mLoadButton);
		
		setFooter(box);
		
		mLoadButton.addClickListener(this);
		
		List<String> regions = new ArrayList<String>();
		
		for (String region : mGroup) {
			regions.add(region);
		}
		
		mTextArea.setText(regions);
	}
	
	/* (non-Javadoc)
	 * @see java.awt.Component#getName()
	 */
	public String getName() {
		return mNameField.getText();
	}
	
	/**
	 * Gets the color.
	 *
	 * @return the color
	 */
	public Color getColor() {
		return mColorButton.getSelectedColor();
	}
	
	/**
	 * Gets the entries.
	 *
	 * @return the entries
	 */
	public List<String> getEntries() {
		return mTextArea.getLines();
	}
	
	/**
	 * Open files.
	 *
	 * @throws Exception the exception
	 */
	public void openFiles() throws Exception {
		openFiles(RecentFilesService.getInstance().getPwd());
	}

	/**
	 * Open files.
	 *
	 * @param pwd the pwd
	 * @throws Exception the exception
	 */
	public void openFiles(Path pwd) throws Exception {
		openFiles(FileDialog.open(mParent).filter(
				new AllXlsxGuiFileFilter(),
				new XlsxGuiFileFilter(),
				new TxtGuiFileFilter(),
				new BedGuiFileFilter(),
				new BedGraphGuiFileFilter()).getFiles(pwd));
	}

	/**
	 * Open files.
	 *
	 * @param files the files
	 * @throws Exception the exception
	 */
	public void openFiles(List<Path> files) throws Exception {
		if (files == null) {
			return;
		}

		for (Path file : files) {

			if (BioPathUtils.ext().bedgraph().test(file)) { //getFileExt(file).equals("bedgraph")) {
				List<UCSCTrack> bedGraphs = BedGraph.parse(file);

				List<GenomicRegion> regions = new ArrayList<GenomicRegion>();
				
				for (UCSCTrack bedGraph : bedGraphs) {
					for (UCSCTrackRegion region :  bedGraph.getRegions()) {
						regions.add(region);
					}
				}
			
				mTextArea.setRegions(regions);
			} else if (BioPathUtils.ext().bed().test(file)) { //PathUtils.getFileExt(file).equals("bed")) {
				mTextArea.setRegions(Bed.parseTracks(file).get(0));
			} else {
				//mTextArea.setRegions(GenomicRegion.parse(Excel.getTextFromFile(file, true)));
				
				mTextArea.setText(Excel.getTextFromFile(file, true));
			}

			RecentFilesService.getInstance().add(file);
		}
	}

	/* (non-Javadoc)
	 * @see org.abh.common.ui.ui.event.ModernClickListener#clicked(org.abh.common.ui.ui.event.ModernClickEvent)
	 */
	@Override
	public void clicked(ModernClickEvent e) {
		try {
			openFiles();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}
}
