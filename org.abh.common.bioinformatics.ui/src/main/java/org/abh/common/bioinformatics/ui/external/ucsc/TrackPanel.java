/**
 * Copyright (C) 2016, Antony Holmes
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *  3. Neither the name of copyright holder nor the names of its contributors 
 *     may be used to endorse or promote products derived from this software 
 *     without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.abh.common.bioinformatics.ui.external.ucsc;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.Box;

import org.abh.common.bioinformatics.ext.ucsc.UCSCTrack;
import org.abh.common.ui.event.ModernClickEvent;
import org.abh.common.ui.event.ModernClickListener;
import org.abh.common.ui.graphics.color.ColorSwatchButton;
import org.abh.common.ui.panel.HBox;
import org.abh.common.ui.panel.MatrixPanel;
import org.abh.common.ui.panel.ModernPanel;
import org.abh.common.ui.spinner.ModernCompactSpinner;
import org.abh.common.ui.text.ModernAutoSizeLabel;
import org.abh.common.ui.text.ModernClipboardTextField;
import org.abh.common.ui.text.ModernTextBorderPanel;
import org.abh.common.ui.text.ModernTextField;
import org.abh.common.ui.widget.ModernWidget;
import org.abh.common.ui.window.ModernWindow;

// TODO: Auto-generated Javadoc
/**
 * Allows a matrix group to be edited.
 *
 * @author Antony Holmes Holmes
 */
public class TrackPanel extends ModernPanel {
	
	/**
	 * The constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * The member color button.
	 */
	private ColorSwatchButton mColorButton;

	/**
	 * The member name field.
	 */
	private ModernTextField mNameField = new ModernClipboardTextField("Name");
	
	/**
	 * The member description field.
	 */
	private ModernTextField mDescriptionField = new ModernClipboardTextField("Description");
	
	/**
	 * The member height field.
	 */
	private ModernCompactSpinner mHeightField = new ModernCompactSpinner(1, 128);

	/**
	 * The member track.
	 */
	private UCSCTrack mTrack;

	/**
	 * The class ClickEvents.
	 */
	private class ClickEvents implements ModernClickListener {

		/* (non-Javadoc)
		 * @see org.abh.common.ui.ui.event.ModernClickListener#clicked(org.abh.common.ui.ui.event.ModernClickEvent)
		 */
		@Override
		public void clicked(ModernClickEvent e) {
			edit();
		}
		
	}
	
	/**
	 * The class KeyEvents.
	 */
	private class KeyEvents implements KeyListener {

		/* (non-Javadoc)
		 * @see java.awt.event.KeyListener#keyPressed(java.awt.event.KeyEvent)
		 */
		@Override
		public void keyPressed(KeyEvent e) {
			// TODO Auto-generated method stub
			
		}

		/* (non-Javadoc)
		 * @see java.awt.event.KeyListener#keyReleased(java.awt.event.KeyEvent)
		 */
		@Override
		public void keyReleased(KeyEvent e) {
			edit();
		}

		/* (non-Javadoc)
		 * @see java.awt.event.KeyListener#keyTyped(java.awt.event.KeyEvent)
		 */
		@Override
		public void keyTyped(KeyEvent e) {
			
		}
		
	}
	
	/**
	 * Instantiates a new track panel.
	 *
	 * @param parent the parent
	 * @param track the track
	 */
	public TrackPanel(ModernWindow parent, UCSCTrack track) {
		mTrack = track;
		
		mNameField.setText(track.getName());
		mDescriptionField.setText(track.getDescription());
		mHeightField.setValue(track.getHeight());

		//this.getContentPane().add(new JLabel("Change " + getProductDetails().getProductName() + " settings", JLabel.LEFT), BorderLayout.PAGE_START);

		int[] rows = {ModernWidget.WIDGET_HEIGHT};
		int[] cols = {120, 400};
		
		MatrixPanel matrixPanel = new MatrixPanel(rows, 
				cols, 
				ModernWidget.PADDING, 
				ModernWidget.PADDING);
		
		mColorButton = new ColorSwatchButton(parent, 
				mTrack.getColor());

		matrixPanel.add(new ModernAutoSizeLabel("Name"));
		matrixPanel.add(new ModernTextBorderPanel(mNameField));
		matrixPanel.add(new ModernAutoSizeLabel("Description"));
		matrixPanel.add(new ModernTextBorderPanel(mDescriptionField));
		matrixPanel.add(new ModernAutoSizeLabel("Color"));
		
		Box box = HBox.create();
		box.add(mColorButton);
		
		matrixPanel.add(box);
		
		matrixPanel.add(new ModernAutoSizeLabel("Height (pixels)"));
		
		box = HBox.create();
		
		//ModernTextPanel panel = new ModernTextPanel(mHeightField);
		//Ui.setSize(panel, new Dimension(100, 24));
		
		box.add(mHeightField);
		
		matrixPanel.add(box);
		
		matrixPanel.setBorder(ModernWidget.DOUBLE_BORDER);

		add(matrixPanel);
		
		mColorButton.addClickListener(new ClickEvents());
		mNameField.addKeyListener(new KeyEvents());
		mDescriptionField.addKeyListener(new KeyEvents());
		mHeightField.addKeyListener(new KeyEvents());
	}


	
	/**
	 * Edits the.
	 */
	private void edit() {
		mTrack.setName(mNameField.getText());
		mTrack.setDescription(mDescriptionField.getText());
		mTrack.setColor(mColorButton.getSelectedColor());
		mTrack.setHeight(mHeightField.getIntValue());
	}
	
	/**
	 * Gets the track.
	 *
	 * @return the track
	 */
	public UCSCTrack getTrack() {
		return mTrack; //new MatrixGroup(nameField.getText(), regexes, colorButton.getSelectedColor());
	}
}
