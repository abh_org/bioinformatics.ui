/**
 * Copyright (C) 2016, Antony Holmes
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *  3. Neither the name of copyright holder nor the names of its contributors 
 *     may be used to endorse or promote products derived from this software 
 *     without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.abh.common.bioinformatics.ui.motifs;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.swing.Box;

import org.abh.common.bioinformatics.motifs.Motif;
import org.abh.common.bioinformatics.motifs.MotifsDataSourceService;
import org.abh.common.event.ChangeEvent;
import org.abh.common.text.Splitter;
import org.abh.common.text.TextUtils;
import org.abh.common.tree.TreeNode;
import org.abh.common.tree.TreeRootNode;
import org.abh.common.ui.ModernComponent;
import org.abh.common.ui.UIService;
import org.abh.common.ui.button.ModernButton;
import org.abh.common.ui.dialog.ModernDialogFlatButton;
import org.abh.common.ui.dialog.ModernMessageDialog;
import org.abh.common.ui.event.ModernClickEvent;
import org.abh.common.ui.event.ModernClickListener;
import org.abh.common.ui.event.ModernSelectionListener;
import org.abh.common.ui.graphics.icons.MinusVectorIcon;
import org.abh.common.ui.graphics.icons.PlusVectorIcon;
import org.abh.common.ui.panel.HBox;
import org.abh.common.ui.panel.ModernContentPanel;
import org.abh.common.ui.scrollpane.ModernScrollPane;
import org.abh.common.ui.scrollpane.ScrollBarPolicy;
import org.abh.common.ui.search.ModernSearchExtPanel;
import org.abh.common.ui.tree.ModernTree;
import org.abh.common.ui.tree.ModernTreeEvent;
import org.abh.common.ui.tree.TreeEventListener;
import org.abh.common.ui.widget.ModernWidget;
import org.abh.common.ui.window.ModernWindow;

// TODO: Auto-generated Javadoc
/**
 * The class MotifsTreePanel.
 */
public class MotifsTreePanel extends ModernComponent {
	
	/**
	 * The constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * The constant NO_MOTIFS.
	 */
	private static final List<Motif> NO_MOTIFS = 
			Collections.unmodifiableList(new ArrayList<Motif>());

	/**
	 * Determines whether to expand or collapse the motif tree based on
	 * how many child nodes are in the tree.
	 */
	private static final int EXPAND_THRESHOLD = 100;

	/**
	 * The member tree.
	 */
	private ModernTree<Motif> mTree = new ModernTree<Motif>();
	
	/**
	 * The member expand button.
	 */
	private ModernButton mExpandButton = 
			new ModernDialogFlatButton(UIService.getInstance().loadIcon(PlusVectorIcon.class, 16));

	/**
	 * The member collapse button.
	 */
	private ModernButton mCollapseButton = 
			new ModernDialogFlatButton(UIService.getInstance().loadIcon(MinusVectorIcon.class, 16));

	/**
	 * The member refresh button.
	 */
	private ModernButton mRefreshButton =
			new ModernDialogFlatButton(UIService.getInstance().loadIcon("refresh", 16));
	
	//private ModernButton mSearchButton =
	//		new ModernButton(UIResources.getInstance().loadIcon("search", 16));

	/**
	 * The member search panel.
	 */
	private ModernSearchExtPanel mSearchPanel;
	
	/**
	 * The member model.
	 */
	private MotifModel mModel;

	/** The m window. */
	private ModernWindow mWindow;

	//private boolean mState = true;

	/**
	 * The class TreeEvents.
	 */
	private class TreeEvents implements TreeEventListener {

		/* (non-Javadoc)
		 * @see org.abh.common.ui.ui.tree.TreeEventListener#treeNodeDragged(org.abh.common.ui.ui.tree.ModernTreeEvent)
		 */
		@Override
		public void treeNodeDragged(ModernTreeEvent e) {
			// TODO Auto-generated method stub

		}

		/* (non-Javadoc)
		 * @see org.abh.common.ui.ui.tree.TreeEventListener#treeNodeClicked(org.abh.common.ui.ui.tree.ModernTreeEvent)
		 */
		@Override
		public void treeNodeClicked(ModernTreeEvent e) {
			if (mModel != null) {
				mModel.set(getSelectedMotifs());
			}
		}

		/* (non-Javadoc)
		 * @see org.abh.common.ui.ui.tree.TreeEventListener#treeNodeDoubleClicked(org.abh.common.ui.ui.tree.ModernTreeEvent)
		 */
		@Override
		public void treeNodeDoubleClicked(ModernTreeEvent e) {
			// TODO Auto-generated method stub

		}

	}

	/**
	 * The class RefreshEvents.
	 */
	public class RefreshEvents implements ModernClickListener {

		/* (non-Javadoc)
		 * @see org.abh.common.ui.ui.event.ModernClickListener#clicked(org.abh.common.ui.ui.event.ModernClickEvent)
		 */
		@Override
		public void clicked(ModernClickEvent e) {
			try {
				refresh();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}

	}
	
	/**
	 * The class CollapseEvents.
	 */
	public class CollapseEvents implements ModernClickListener {

		/* (non-Javadoc)
		 * @see org.abh.common.ui.ui.event.ModernClickListener#clicked(org.abh.common.ui.ui.event.ModernClickEvent)
		 */
		@Override
		public void clicked(ModernClickEvent e) {
			setState(false);
		}
	}
	
	/**
	 * The class ExpandEvents.
	 */
	public class ExpandEvents implements ModernClickListener {

		/* (non-Javadoc)
		 * @see org.abh.common.ui.ui.event.ModernClickListener#clicked(org.abh.common.ui.ui.event.ModernClickEvent)
		 */
		@Override
		public void clicked(ModernClickEvent e) {
			setState(true);
		}
	}
	
	/**
	 * The Class SelectionEvents.
	 */
	private class SelectionEvents implements ModernSelectionListener {

		/* (non-Javadoc)
		 * @see org.abh.common.ui.event.ModernSelectionListener#selectionChanged(org.abh.common.event.ChangeEvent)
		 */
		@Override
		public void selectionChanged(ChangeEvent e) {
			mModel.set(getSelectedMotifs());
		}
		
	}

	/**
	 * Instantiates a new motifs tree panel.
	 *
	 * @param window the window
	 */
	public MotifsTreePanel(ModernWindow window) {
		this(window, new MotifModel());
	}
	
	/**
	 * Instantiates a new motifs tree panel.
	 *
	 * @param window the window
	 * @param model the model
	 */
	public MotifsTreePanel(ModernWindow window, MotifModel model) {
		mWindow = window;
		mModel = model;
		
		mSearchPanel = new ModernSearchExtPanel(window);
		
		setup();

		createUi();

		mRefreshButton.addClickListener(new RefreshEvents());
		mCollapseButton.addClickListener(new CollapseEvents());
		mExpandButton.addClickListener(new ExpandEvents());
		mSearchPanel.addClickListener(new RefreshEvents());

		try {
			refresh();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Search.
	 *
	 * @param items the items
	 */
	public void search(Collection<String> items) {
		mSearchPanel.search(items);
	}

	/**
	 * Creates the ui.
	 */
	public void createUi() {
		ModernComponent c = new ModernComponent(mSearchPanel);
		c.setBorder(ModernWidget.BOTTOM_BORDER);
		setHeader(c);
		

		ModernScrollPane scrollPane = new ModernScrollPane(mTree)
				.setHorizontalScrollBarPolicy(ScrollBarPolicy.NEVER);
		//scrollPane.setVerticalScrollBarPolicy(ScrollBarPolicy.ALWAYS);
		//Ui.setSize(scrollPane, new Dimension(250, Short.MAX_VALUE));

		//scrollPane.setMinimumSize(new Dimension(100, Short.MAX_VALUE));
		//scrollPane.setPreferredSize(new Dimension(250, Short.MAX_VALUE));
		//scrollPane.setMaximumSize(new Dimension(800, Short.MAX_VALUE));

		//ModernLineBorderPanel panel = new ModernLineBorderPanel(scrollPane);
		
		//panel.setBorder(TOP_BORDER);
		setBody(scrollPane);

		/*
		Box box = HBox.create();
		box.setBorder(TOP_BORDER);
		mRefreshButton.setToolTip("Refresh", "Refresh the database.");
		box.add(mRefreshButton);
		box.add(createHGap());
		mExpandButton.setToolTip("Expand", "Expand all folders.");
		box.add(mExpandButton);
		box.add(createHGap());
		mCollapseButton.setToolTip("Collapse", "Collapse all folders.");
		box.add(mCollapseButton);
		add(box, BorderLayout.PAGE_END);
		*/
	}

	/**
	 * Setup.
	 */
	private void setup() {
		mTree.addTreeListener(new TreeEvents());
		
		mTree.addSelectionListener(new SelectionEvents());
	}

	/**
	 * Generate a tree view of a sample folder and its sub folders.
	 *
	 * @throws Exception the exception
	 */
	public void refresh() throws Exception {
		TreeRootNode<Motif> root = new TreeRootNode<Motif>();
		
		List<String> terms = Splitter.on(TextUtils.COMMA_DELIMITER)
				.trim()
				.ignoreEmptyStrings()
				.text(mSearchPanel.getText());
		
		TreeNode<Motif> node = new TreeNode<Motif>("Motifs");
		
		MotifsDataSourceService.getInstance().createTree(node, 
				terms,
				mSearchPanel.getInList(),
				mSearchPanel.getExact(),
				mSearchPanel.getCaseSensitive());
		
		root.addChild(node);
		
		mTree.setRoot(root);
		
		// The motifs node is always expanded
		node.updateExpanded(true);
		// The motifs node's children are expanded on condition there are
		// fewer than EXPAND_THRESHOLD
		node.updateChildrenAreExpanded(node.getCumulativeChildCount() <= EXPAND_THRESHOLD, true);
		node.fireTreeNodeChanged();
		
		if (mTree.getRoot().getCumulativeChildCount() == 0) {
			ModernMessageDialog.createInformationDialog(mWindow, "No motifs were found.");
		}
	}
	
	//private void setState() {
	//	setState(mState);
	//}
	
	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	private void setState(boolean state) {
		//mState = state;
		
		mTree.getRoot().setChildrenAreExpanded(state);
	}

	/**
	 * Sets the selected.
	 *
	 * @param i the new selected
	 */
	public void setSelected(int i) {
		mTree.selectNode(i);
	}

	/**
	 * Gets the selected motifs.
	 *
	 * @return the selected motifs
	 */
	public List<Motif> getSelectedMotifs() {
		if (mTree.getSelectedNodes().size() == 0) {
			return NO_MOTIFS; //new ArrayList<ExperimentSearchResult>();
		}

		List<Motif> motifs = new ArrayList<Motif>();

		for (TreeNode<Motif> node : mTree.getSelectedNodes()) {
			selectedMotifs(node, motifs);
		}

		return motifs;
	}

	/**
	 * Recursively examine a node and its children to find those with experiments.
	 *
	 * @param node the node
	 * @param motifs the motifs
	 */
	private void selectedMotifs(TreeNode<Motif> node, 
			List<Motif> motifs) {
		if (node.getValue() != null) {
			motifs.add(node.getValue());
		}

		for (TreeNode<Motif> child : node) {
			selectedMotifs(child, motifs);
		}
	}
}
